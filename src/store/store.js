import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import { combineEpics, createEpicMiddleware } from 'redux-observable'

import hangman from './reducers'
import epics from './epics'

const epicMiddleware = createEpicMiddleware()

const middleware = [
  epicMiddleware
]

export const rootEpic = combineEpics(
  ...epics
)

const reducers = combineReducers({
  hangman
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  reducers,
  {},
  composeEnhancers(applyMiddleware(...middleware)),
)

epicMiddleware.run(rootEpic)
export default store
