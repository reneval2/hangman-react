import { actionTypes } from './actions'
import createReducers from './createReducers'
import * as R from 'ramda'
import { GAME_STATUS } from '../constants/const'


const initialState = {
  letters: [],
  step: 0,
  status: GAME_STATUS.NOT_STARTED,
  word: '',
}

const handlers = {
  [actionTypes.KEY_PRESSED]: (state, { key }) => {
    return R.evolve(
      {
        letters: R.append(R.toUpper(key)),
        step: R.when(
          R.converge(
            R.complement(R.includes), [
              R.always(key),
              R.always(
                R.prop('word', state))
            ]),
          R.inc
        )
      },
      state,
    )
  },

  [actionTypes.WORD_RECEIVED]: (state, { word }) => {
    return R.evolve(
      {
        letters: R.always([]),
        step: R.always(0),
        word: R.always(R.toUpper(word)),
        status: R.always(GAME_STATUS.IN_PROGRESS),
      },
      state,
    )
  },
  [actionTypes.START_GAME]: (state ) => {
    return R.evolve(
      {
        status: R.always(GAME_STATUS.LOADING),
      },
      state,
    )
  },
  [actionTypes.END_GAME]: (state, { status }) => {
    return R.evolve(
      {
        status: R.always(status),
      },
      state,
    )
  },
}

export default createReducers(initialState, handlers)