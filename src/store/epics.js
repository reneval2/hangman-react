import { actionTypes, setGameStatus, nextTurn, wordReceived } from './actions'
import { map, filter, takeUntil, switchMap, withLatestFrom, catchError } from 'rxjs/operators'
import { ajax } from 'rxjs/ajax'
import { of, fromEvent } from 'rxjs'
import { ofType, } from 'redux-observable'
import * as R from 'ramda'
import { selectAllLettersGuessed, selectLetters, selectStep } from './selectors'
import { API, GAME_STATUS } from '../constants/const'

const API_KEY = process.env.REACT_APP_API_KEY
const API_HOST = process.env.REACT_APP_API_HOST

const newWordRequest = ajax({
    url: API.URL + API.WORD_PATTERN,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-RapidAPI-Key': API_KEY,
      'X-RapidAPI-Host': API_HOST,
    }
  }
)

const startListenKeys = (action$, state$) => fromEvent(window, 'keyup').pipe(
  filter((event) => event.key && event.key.length === 1 && /[a-zA-Z]/i.test(event.key)),
  withLatestFrom(state$),
  filter(([event, state]) => R.complement(R.includes)(R.toUpper(event.key), selectLetters(state))),
  map(([event]) => ({
    type: actionTypes.KEY_PRESSED,
    key: R.toUpper(event.key),
    isChar: /[a-zA-Z]/i.test(event.key),
    event
  })),
  takeUntil(action$.ofType(actionTypes.END_GAME)),
)

const startGameEpic = (action$) =>
  action$.pipe(
    ofType(actionTypes.START_GAME),
    switchMap(() =>
      newWordRequest.pipe(
        switchMap(({ response: { word } }) => of(wordReceived({ word }))),
        catchError(error => {
          console.log('error: ', error)
          return of(setGameStatus({ status: GAME_STATUS.ERROR }))
        })
      )
    )
  )

const wordReceivedEpic = (action$, state$) =>
  action$.pipe(
    ofType(actionTypes.WORD_RECEIVED),
    switchMap(() => startListenKeys(action$, state$))
  )


const nextTurnEpic = (action$, state$) =>
  action$.pipe(
    ofType(actionTypes.KEY_PRESSED),
    withLatestFrom(state$),
    switchMap(([, state]) => {
      const step = selectStep(state)
      if (step > 10) {
        return of(setGameStatus({ status: GAME_STATUS.FINISHED_FAIL }))
      }
      if (selectAllLettersGuessed(state)) {
        return of(setGameStatus({ status: GAME_STATUS.FINISHED_WIN }))
      }
      return of(nextTurn())
    })
  )


export default [
  startGameEpic,
  nextTurnEpic,
  wordReceivedEpic
]