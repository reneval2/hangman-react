import { createSelector } from 'reselect'
import * as  R from 'ramda'

const transformWordToArray = R.pipe(
  R.toUpper,
  R.split(''),
  R.chain(
    R.concat,
    R.pipe(
      R.length,
      R.subtract(11),
      R.pipe(
        R.range(0),
        R.map(R.always('*'))
      )
    )
  )
)

const maskWord = (word, letters) => {
  return R.map(
    R.cond([
        [R.flip(R.includes)(letters), R.identity],
        [R.equals('*'), R.identity],
        [R.T, R.always('')]
      ]
    )
  )(word)
}

export const selectHangmanSector = R.path(['hangman'])

export const selectLetters = createSelector(
  selectHangmanSector,
  R.prop('letters'),
)

export const selectWord = createSelector(
  selectHangmanSector,
  R.prop('word')
)
export const selectTransformedWord = createSelector(
  selectWord,
  transformWordToArray
)

export const selectStep = createSelector(
  selectHangmanSector,
  R.prop('step'),
)
export const selectStatus = createSelector(
  selectHangmanSector,
  R.prop('status'),
)

export const selectMaskedWord = createSelector(
  selectTransformedWord,
  selectLetters,
  maskWord,
)

export const selectWrongLetters = createSelector(
  selectTransformedWord,
  selectLetters,
  (word, letters) => R.filter(R.complement(R.flip(R.includes))(word), letters)
)


export const selectAllLettersGuessed = createSelector(
  selectTransformedWord,
  selectLetters,
  (word, letters) => R.all(
    R.either(
      R.flip(R.includes)(letters),
      R.equals('*'),
    ),
    word
  )
)
