export const actionTypes = {
  START_GAME: 'START_GAME',
  END_GAME: 'END_GAME',
  KEY_PRESSED: 'KEY_PRESSED',
  WORD_RECEIVED: 'WORD_RECEIVED',
  NEXT_TURN: 'NEXT_TURN',
}

export function startGame() {
  return {
    type: actionTypes.START_GAME
  }
}

export function setGameStatus({status}) {
  return {
    type: actionTypes.END_GAME,
    status
  }
}
export function wordReceived({word}) {
  return {
    type: actionTypes.WORD_RECEIVED,
    word
  }
}

export function nextTurn() {
  return {
    type: actionTypes.NEXT_TURN
  }
}
