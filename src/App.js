import React from 'react'
import './App.scss'

import { Provider } from 'react-redux'
import store from './store/store'
import Game from './componenents/Game/Game'

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Game />
      </div>
    </Provider>
  )
}

export default App
