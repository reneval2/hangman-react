export const GAME_STATUS = {
  NOT_STARTED: 'NOT_STARTED',
  IN_PROGRESS: 'IN_PROGRESS',
  FINISHED_FAIL: 'FINISHED_FAIL',
  FINISHED_WIN: 'FINISHED_WIN',
  LOADING: 'LOADING',
}

export const API = {
  URL: 'https://wordsapiv1.p.rapidapi.com/words/?random=true&lettersMax=11&partOfSpeech=noun&letterPattern=',
  WORD_PATTERN: '^\\w{2,11}$',
}

