import React from 'react'
import './Word.scss'
import * as R from 'ramda'
import uuid from 'uuid/v4'
const generateKey = (pre) => {
  return `${ pre }_${uuid()}`
}


const Word = ({ word }) => {
  return (
    <div className='words'>
      {
        word.map(R.cond([
            [R.equals('*'),R.always(<div className='letter not-active'></div>)],
            [R.T, letter => <div className='letter' key={generateKey('word')}>{letter}</div>]
          ]
        ))
      }
    </div>
  )
}

export default Word