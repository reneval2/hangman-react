import React from 'react'
import './GameOverlay.scss'
import { GAME_STATUS } from '../../constants/const'

const GameOverlay = ({ onStartGame, status, word }) => {

  if (![GAME_STATUS.FINISHED_WIN,
    GAME_STATUS.FINISHED_FAIL,
    GAME_STATUS.NOT_STARTED,
    GAME_STATUS.LOADING,
    GAME_STATUS.ERROR
  ].includes(status)) {
    return null
  }

  if (status === GAME_STATUS.LOADING) {
    return <div className='gameOver'>Loading...</div>
  }
  return (
    <div className='gameOver'>
      {![GAME_STATUS.NOT_STARTED, GAME_STATUS.ERROR].includes(status) && <div> Game over</div>}
      {status === GAME_STATUS.FINISHED_FAIL && <div className="small-text">Word was: {word} </div>}
      {status === GAME_STATUS.FINISHED_WIN && <div className="small-text">Congratulation!</div>}
      {status === GAME_STATUS.ERROR && <div>Error </div>}
      <button className="new-word" onClick={onStartGame}>
        {status === GAME_STATUS.ERROR ? 'Retry' : 'New word'}
      </button>
    </div>
  )
}

export default GameOverlay