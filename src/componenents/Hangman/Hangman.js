import React from 'react'
import './Hangman.scss'

const Hangman = ({ step }) => {
  return (
    <div className={`step-${step}`} />
  )
}
export default Hangman