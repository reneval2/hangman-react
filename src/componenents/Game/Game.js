import React from 'react'
import { connect } from 'react-redux'
import * as R from 'ramda'

import Hangman from '../Hangman/Hangman'
import { startGame } from '../../store/actions'
import barImage from '../../assets/bar.png'
import {
  selectMaskedWord,
  selectStatus,
  selectStep,
  selectWord,
  selectWrongLetters
} from '../../store/selectors'
import './Game.scss'
import Word from '../Word/Word'
import MissedLetters from '../MissedLetters/MissedLetters'
import GameOverlay from '../GameOverlay/GameOverlay'


const Game = ({ startGame, word, step, letters, status,wordUnmasked }) => {
  return (
    <div className="game-field">
      <div className="wrapper">
        <div className="cropped-field">
          <div className="top-section">
            <div className="hangman-image">
              <img className="bar-img" src={barImage} alt="hangman"></img>
              <Hangman step={step} />
            </div>
            <div className="right-section">
              {R.complement(R.isEmpty)(letters) && <div>You missed:</div>}
              <MissedLetters letters={letters} />
            </div>
          </div>
          <div className="bottom-section">
            <Word word={word} />
          </div>
        </div>
        <div className="ribbon" />
      </div>
      <GameOverlay word={wordUnmasked} status={status} onStartGame={startGame} />
    </div>
  )
}

const mapStateToProps = (state) => ({
  letters: selectWrongLetters(state),
  word: selectMaskedWord(state),
  wordUnmasked: selectWord(state),
  step: selectStep(state),
  status: selectStatus(state)
})

const mapDispathToProps = {
  startGame
}

export default connect(
  mapStateToProps,
  mapDispathToProps
)(Game)
