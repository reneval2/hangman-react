import React from 'react'
import './MissedLetters.scss'

const MissedLetters = ({ letters }) => (
    <div className='missed-letters'>
     {letters.map(letter=><div>{letter}</div>)}
    </div>
)

export default MissedLetters